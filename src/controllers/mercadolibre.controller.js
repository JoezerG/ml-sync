const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');
const open = require('open');
const express = require('express');

const config = require('../../config/config');

const app = express();
const server = app.listen(3333);

app.get('/auth', (req, res) => {
  const authCode = req.query.code;
  if (authCode) {
    res.end('Ya puedes Cerrar esta ventana');
    getAuth(authCode);
  }
})

const mlAuth = () => {
  const authURL = `https://auth.mercadolibre.com.ve/authorization?response_type=code&client_id=${config.ml_id}`;
  server;
  open(authURL);
}

const getAuth = (token) => {
  const tokenURL = `https://api.mercadolibre.com/oauth/token?grant_type=authorization_code&client_id=${config.ml_id}&client_secret=${config.ml_secret}&code=${token}&redirect_uri=http://localhost:3333/auth`;
  fetch(tokenURL, {
      method: 'POST'
    })
    .then(res => res.json())
    .then(authObj => {
      fs.writeFile(path.join(__dirname, '../../config/accessml.json'), JSON.stringify(authObj), () => server.close());
      console.log('Exito, su session de Mercadolibre es valida por 6 Horas');
    });
}

const createProduct = (product) => {
  const token = require('../../config/accessml.json');
  const productURL = `https://api.mercadolibre.com/items?access_token=${token.access_token}`;
  return new Promise((resolve, reject) => {
    fetch(productURL, {
        method: 'POST',
        body: JSON.stringify(product)
      })
      .then(res => res.json())
      .then(obj => {
        if (obj.status == 400) {
          reject(obj)
        } else {
          console.log(`${product.title} OK`);
          resolve(obj)
        }
      })
  })
}

const updateProduct = (id, product) => {
  const token = require('../../config/accessml.json');
  const productURL = `https://api.mercadolibre.com/items/${id}?access_token=${token.access_token}`;
  return new Promise((resolve, reject) => {
    fetch(productURL, {
      method: 'PUT',
      body: JSON.stringify(product)
    })
    .then(res => res.json())
    .then(obj => {
      if(obj.status == 400) {
        reject(obj)
      } else {
        //console.log(`${product.title} OK`);
        resolve(obj);
      }
    })
  })
}

module.exports = {
  mlAuth,
  createProduct,
  updateProduct
}