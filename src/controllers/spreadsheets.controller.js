const spreadsheet = require('google-spreadsheet');
const {
  extractSheets
} = require('spreadsheet-to-json');
const {
  promisify
} = require('util')
const _ = require('lodash');


const config = require('../../config/config');
const credentials = require('../../config/services-account.json');

const getList = () => {
  return new Promise((resolve, reject) => {
    extractSheets({
        spreadsheetKey: config.id_spreadsheet,
        credentials: credentials,
        sheetsToExtract: ["Inventario_ML"]
      },
      (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data.Inventario_ML);
        }
      }
    );
  })
}
/** {
  @description Function para actualizacion de celda por referencias
  @param obj: { key de refencia de busqueda,
                refValue: valor de referencia de busqueda,
                refUpdate: key de referencia para actualizar,
                refUpdateValue: valor a actualizar } 
*/
const updateCell = async (data) => {
  const doc = new spreadsheet(config.id_spreadsheet);
  await promisify(doc.useServiceAccountAuth)(credentials);
  const info = await promisify(doc.getInfo)();
  const sheet = info.worksheets[0];
  await promisify(sheet.getRows)({
    offset: 1,
  }, (err, rows) => {
    if (err) {
      console.error(err);
    } else {
      let row = _.find(rows, {title: data.refValue});
      row.mlstatus = data.status;
      row.idml = data.id;
      row.save();
    };
  })
}

module.exports = {
  getList,
  updateCell
}