const argv = require('yargs');
const fs = require('fs');
const path = require('path');

const spreadsheet = require('./controllers/spreadsheets.controller');
const ml = require('./controllers/mercadolibre.controller');

if (argv.argv.subir) {
  if (fs.existsSync(path.join(__dirname, '../config/accessml.json'))) {
    spreadsheet.getList().then(list => {
      list.forEach(item => {
        if (item.ml_status === 'FALSE') {
          console.log(`Creando Producto ${item.title}`)
          let product = {
            "title": item.title,
            "category_id": item.ml_category,
            "price": item.price,
            "currency_id": "VES",
            "condition": "new",
            "available_quantity": item.cant,
            "buying_mode": "buy_it_now",
            "listing_type_id": "gold_special",
            "description": {
              'plain_text': item.description
            },
            "video_id": item.id_video || '',
            "pictures": []
          };

          var imgs_url = item.images_url.split(';');

          imgs_url.forEach(e => {
            product.pictures.push({
              source: `https://nyc3.digitaloceanspaces.com/besalel-1/decobec${e}`
            })
          })
          ml.createProduct(product).then(product_ml => {
            spreadsheet.updateCell({
              ref: 'title',
              refValue: item.title,
              status: true,
              id: product_ml.id
            }).then(() => console.log(`OK ${item.title} CODE_ML: ${product_ml.id}`))
          }).catch(e => console.error(e));
        }
      });
    });
  } else {
    console.log('Porfavor ejecute el comando "--login"');
    process.exit()
  }
} else if (argv.argv.login) {
  ml.mlAuth()
} else if (argv.argv.actualizar) {
  if (fs.existsSync(path.join(__dirname, '../config/accessml.json'))) {
    spreadsheet.getList().then(list => {
      list.forEach(item => {
        if (item.ml_status === 'TRUE') {
          console.log(`Actualizando Producto ${item.title}`)
          let product = {
            "price": item.price
          };
          ml.updateProduct(item.id_ml, product).then(product_ml => {
            console.log(`OK ${product_ml.title} CODE_ML: ${product_ml.id}`)
          }).catch(e => console.error(e));
        }
      });
    });
  } else {
    console.log('Porfavor ejecute el comando "--login"');
    process.exit()
  }
}